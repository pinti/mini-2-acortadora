from django.db import models

class Contenido(models.Model):
    url = models.TextField()
    url_acortada = models.TextField()
