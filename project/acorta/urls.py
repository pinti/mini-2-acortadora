from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path("", views.form_and_list),
    path("<str:url_acortada>", views.mirar_contenido),
]
