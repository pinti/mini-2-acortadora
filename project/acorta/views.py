from django.shortcuts import render
from django.http import HttpResponse
from .models import Contenido
from django.views.decorators.csrf import csrf_exempt
import urllib.parse
from django.template import loader


@csrf_exempt
def form_and_list(request):
    contenidos = Contenido.objects.all()

    if request.method == "GET":
        template = loader.get_template("form.html")
    context = {"contenidos":contenidos}

    if request.method == "POST":
        peticion_partida = request.body.decode("utf-8").split("&")
        print("peticion partida: ", peticion_partida);

        if peticion_partida[0] == "url_original=" and peticion_partida[1] == "url_acortada=":
            template = loader.get_template("formVacioError.html")
            return HttpResponse(template.render())
        else:
            url = request.POST['url_original']
            print("url" + url)
            if not url.startswith("http://") and not url.startswith("https://"):
                url = "https://" + url
            try:
                url_corta = Contenido.objects.get(url=url)
                print("url corta :" + str(url_corta))
                template = loader.get_template("form.html")
                return HttpResponse(template.render(context, request))

            except Contenido.DoesNotExist:
                acortada = request.POST['url_acortada']
                if not acortada:  
                    count = Contenido.objects.count() + 1
                    acortada = str(count)

                c = Contenido(url=url, url_acortada=acortada)
                c.save()
                template = loader.get_template("form.html")

    return HttpResponse(template.render(context, request))

@csrf_exempt
def mirar_contenido(request, url_acortada):
    contenido = Contenido.objects.get(url_acortada=url_acortada)
    template = loader.get_template("redirect.html")
    context = {"content": contenido}
    return HttpResponse(template.render(context, request))
